public List<AccountDTO> safeFindAccountsByCustomerId(String customerId)
        throws Exception {

        String sql = "select "
        + "customer_id, acc_number, branch_id, balance from Accounts"
        + "where customer_id = ?";

        Connection c = dataSource.getConnection();
        PreparedStatement p = c.prepareStatement(sql);
        p.setString(1, customerId);
        ResultSet rs = p.executeQuery(sql));
        // omitted - process rows and return an account list
        }